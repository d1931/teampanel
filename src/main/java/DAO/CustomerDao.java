/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import Model.Customer;
import Model.Product;
import Model.Stock;
import com.kittikun.Database.Database;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author W.Home
 */
public class CustomerDao implements DaoInterface<Customer>{
    

    @Override
    public int add(Customer object) {
        Connection conn = null;
       Database db = Database.getInstance();
       conn = db.getConnection();
       int id = -1;
        try{
            String sql = "INSERT INTO customer (name,tel,Discount) VALUES(?,?,?)";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, object.getName());
            stmt.setString(2, object.getTel());
            stmt.setInt(3, object.getDiscount());
            int row = stmt.executeUpdate();
            
            ResultSet result = stmt.getGeneratedKeys();   
            
            if(result.next()){
                id = result.getInt(1);
            }
            
            
        } catch (SQLException ex) {
            Logger.getLogger(StockDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        
        return id;
    }

    @Override
    public ArrayList<Customer> getAll() {
        ArrayList<Customer> list = new ArrayList();
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
       try{
           String sql = "SELECT * FROM customer ";
           Statement stmt = conn.createStatement();
           ResultSet result = stmt.executeQuery(sql);
           while(result.next()){
               int cid = result.getInt("id");
               String name = result.getString("name");
               String tel = result.getString("tel");
               int discount = result.getInt("discount");
               
               Customer customer = new Customer(cid,name,tel,discount);
               list.add(customer);
               
           }
       } catch (SQLException ex) {
            Logger.getLogger(CustomerDao.class.getName())
                    .log(Level.SEVERE, null, ex);
        }
       
       db.close();
      
       return list;
    }

    @Override
    public Customer get(int id) {
       Connection conn = null;
       Database db = Database.getInstance();
       conn = db.getConnection();
       try{
           String sql = "SELECT * FROM customer WHERE id ="+id;
           Statement stmt = conn.createStatement();
           ResultSet result = stmt.executeQuery(sql);
           if(result.next()){
               int cid = result.getInt("id");
               String name = result.getString("name");
                String tel = result.getString("tel");
                int discount = result.getInt("discount");
              
               Customer customer = new Customer(cid,name,tel,discount);
               return customer;
               
           }
       } catch (SQLException ex) {
            Logger.getLogger(CustomerDao.class.getName())
                    .log(Level.SEVERE, null, ex);
        }
       
       db.close();
       return null;
    }
    public Customer getByTel(String phn){
       Connection conn = null;
       Database db = Database.getInstance();
       conn = db.getConnection();
       try{
           String sql = "SELECT * FROM customer WHERE tel ="+"\""+phn+"\"";
           Statement stmt = conn.createStatement();
           ResultSet result = stmt.executeQuery(sql);
           if(result.next()){
               int cid = result.getInt("id");
               String name = result.getString("name");
               String tel = result.getString("tel");
               int discount = result.getInt("discount");
              
               Customer customer = new Customer(cid,name,tel,discount);
               return customer;
               
           }
       } catch (SQLException ex) {
            Logger.getLogger(CustomerDao.class.getName())
                    .log(Level.SEVERE, null, ex);
        }
       
       db.close();
       return null;
    }
    public Customer getByName(String nameIn){
       Connection conn = null;
       Database db = Database.getInstance();
       conn = db.getConnection();
       try{
           String sql = "SELECT * FROM customer WHERE name ="+"\""+nameIn+"\"";
           Statement stmt = conn.createStatement();
           ResultSet result = stmt.executeQuery(sql);
           if(result.next()){
               int cid = result.getInt("id");
               String name = result.getString("name");
               String tel = result.getString("tel");
               int discount = result.getInt("discount");
              
               Customer customer = new Customer(cid,name,tel,discount);
               return customer;
               
           }
       } catch (SQLException ex) {
            Logger.getLogger(CustomerDao.class.getName())
                    .log(Level.SEVERE, null, ex);
        }
       
       db.close();
       return null;
    }


    @Override
    public int delete(int id) {
       Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        int row = 0;
        try{
            String sql = "Delete FROM customer WHERE id = ?";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            
            row = stmt.executeUpdate();
            System.out.println("Affect row "+row);
        } catch (SQLException ex) {
            Logger.getLogger(CustomerDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        db.close();
        return row;
    }

    @Override
    public int update(Customer object) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        int row = 0;
        try{
            String sql = "UPDATE customer SET name = ?,tel = ? WHERE id = ?";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, object.getName());
             stmt.setString(2, object.getTel());
            stmt.setInt(3, object.getId());
            row = stmt.executeUpdate();
            System.out.println("Affect row "+row);
        } catch (SQLException ex) {
            Logger.getLogger(CustomerDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        db.close();
        return row;
    }
       
    public static void main(String[] args) {
        CustomerDao dao = new CustomerDao();
//        System.out.println(dao.getAll());
          System.out.println(dao.getByName("wow"));
//        System.out.println(dao.get(1));
//        int id = dao.add(new Customer(-1,"Auu","0991234567"));
//        System.out.println("id ="+id);
//        Customer lastCustomer = dao.get(id);
//        System.out.println(lastCustomer);
//        lastCustomer.setTel("0811111111");
//        int row = dao.update(lastCustomer);
//        Customer UpdateCustomer = dao.get(id);
//        System.out.println("Update   Customer = "+lastCustomer);
//        Customer delC = dao.get(id);
//        System.out.println("Delete Customer "+ delC);
////      dao.delete(id);
//          Customer Up = dao.getByTel("087456");
//        System.out.println("Update   Customer = "+lastCustomer);
////        Customer delC = dao.get(id);
//        System.out.println("Delete Customer "+ Up);
        
        
    }
}
