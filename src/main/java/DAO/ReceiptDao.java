/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import Model.Customer;
import Model.Product;
import Model.Receipt;
import Model.ReceiptDetail;
import Model.Employee;
import com.kittikun.Database.Database;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author TUFGaming
 */
public class ReceiptDao implements DaoInterface<Receipt> {

    @Override
    public int add(Receipt object) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        int id = -1;
        int rdid = -1;
        try {
            String sql = "INSERT INTO receipt (customer_id,employee_id,total,discount) VALUES(?,?,?,?)";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, object.getCustomer().getId());
            stmt.setInt(2, object.getSeller().getId());
            stmt.setDouble(3, object.getTotal());
            stmt.setInt(4, object.getDiscount());
            int row = stmt.executeUpdate();

            ResultSet result = stmt.getGeneratedKeys();

            if (result.next()) {
                id = result.getInt(1);
                object.setId(id);

            }
            for (ReceiptDetail r : object.getReceiptDetail()) {
                String sqlDetail = "INSERT INTO receipt_detail (receipt_id,product_id,price,amount) VALUES(?,?,?,?);";
                PreparedStatement stmtDetail = conn.prepareStatement(sqlDetail);
                stmtDetail.setInt(1, r.getReceipt().getId());
                stmtDetail.setInt(2, r.getProduct().getId());
                stmtDetail.setDouble(3, r.getPrice());
                stmtDetail.setInt(4, r.getAmount());
                int rowDetail = stmtDetail.executeUpdate();
                ResultSet resultDetail = stmt.getGeneratedKeys();
                if (resultDetail.next()) {
                    rdid = resultDetail.getInt(1);
                    r.setId(rdid);
                }
            }

        } catch (SQLException ex) {
            System.out.println("Error to create receipt");
        }
        db.close();

        return id;
    }

    @Override
    public ArrayList<Receipt> getAll() {
        ArrayList list = new ArrayList();
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        try {
            String sql = "SELECT r.id as id,\n"
                    + "       datetime,\n"                    
                    + "       customer_id,\n"
                    + "       c.name as customer_name,\n"
                    + "       c.tel as customer_tel,\n"
                    + "       c.discount as customer_discount,\n"
                    + "       employee_id,\n"
                    + "       u.name as employee_name,\n"
                    + "       r.discount as receipt_discount,\n"
                    + "       total      \n"
                    + "  FROM receipt r, customer c, employee u\n"
                    + "  WHERE r.customer_id = c.id AND r.employee_id = u.id"
                    + "  ORDER BY datetime DESC;";

            Statement stmt = conn.createStatement();
            ResultSet result = stmt.executeQuery(sql);

            while (result.next()) {
                int id = result.getInt("id");
                Date created = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(result.getString("datetime"));                
                int CustomerId = result.getInt("customer_id");
                String customerName = result.getString("customer_name");
                String customerTel = result.getString("customer_tel");
                int customerDiscount = result.getInt("customer_discount");
                int EmployeeId = result.getInt("Employee_id");               
                String employeeName = result.getString("Employee_name");
                int discount = result.getInt("receipt_discount");
                double total = result.getDouble("total");
                
                Receipt receipt = new Receipt(id, created,
                        new Employee(EmployeeId, employeeName),
                        new Customer(CustomerId, customerName, customerTel ,customerDiscount)
                        , discount);
                list.add(receipt);

            }
        } catch (SQLException ex) {
            Logger.getLogger(ProductDao.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ParseException ex) {
            Logger.getLogger(ReceiptDao.class.getName()).log(Level.SEVERE, null, ex);
        }

        db.close();

        return list;
    }

    @Override
    public Receipt get(int id) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        try {
            String sql = "SELECT r.id as id,\n"
                    + "       datetime,\n"
                    + "       customer_id,\n"
                    + "       c.name as customer_name,\n"
                    + "       c.tel as customer_tel,\n"
                    + "       c.discount as customer_discount,\n"
                    + "       employee_id,\n"
                    + "       u.name as employee_name,\n"
                    + "       r.discount as receipt_discount,\n"
                    + "       total      \n"
                    + "  FROM receipt r, customer c, employee u\n"
                    + "  WHERE r.id = ? AND r.customer_id = c.id AND r.employee_id = u.id;";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet result = stmt.executeQuery();
            if (result.next()) {
                int rid = result.getInt("id");
                Date created = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(result.getString("datetime"));
                int CustomerId = result.getInt("customer_id");
                String customerName = result.getString("customer_name");
                String customerTel = result.getString("customer_tel");
                int customerDiscount = result.getInt("customer_discount");
                int EmployeeId = result.getInt("employee_id");
                String employeeName = result.getString("employee_name");
                int discount = result.getInt("receipt_discount");
                double total = result.getDouble("total");
                Receipt receipt = new Receipt(id, created,
                        new Employee(EmployeeId, employeeName),
                        new Customer(CustomerId, customerName, customerTel,customerDiscount),
                        discount);

                getReceiptDetail(conn, id, receipt);

                return receipt;

            }
        } catch (SQLException ex) {
            Logger.getLogger(ProductDao.class.getName())
                    .log(Level.SEVERE, null, ex);
        } catch (ParseException ex) {
            Logger.getLogger(ReceiptDao.class.getName()).log(Level.SEVERE, null, ex);
        }

        db.close();
        return null;
    }

    private void getReceiptDetail(Connection conn, int id, Receipt receipt) throws SQLException {
        String sqlDetail = "SELECT rd.id as id,\n"
                + "       receipt_id,\n"
                + "       product_id,\n"
                + "       p.name as product_name,\n"
                + "       p.price as product_price,\n"
                + "       rd.price as price,       \n"
                + "       amount\n"
                + "  FROM receipt_detail rd , product p\n"
                + "  WHERE receipt_id = ? AND rd.product_id = p.id;";
        PreparedStatement stmtDetail = conn.prepareStatement(sqlDetail);
        stmtDetail.setInt(1, id);
        ResultSet resultDetail = stmtDetail.executeQuery();
        while (resultDetail.next()) {
            int receiveId = resultDetail.getInt("id");
            int producrId = resultDetail.getInt("product_id");
            String productName = resultDetail.getString("product_name");
            double ProductPrice = resultDetail.getDouble("product_price");
            double price = resultDetail.getDouble("price");
            int amount = resultDetail.getInt("amount");
            Product product = new Product(producrId, productName, ProductPrice);
            receipt.addReceiptDetail(receiveId, product, amount, price);

        }
    }

    @Override
    public int delete(int id) {
       Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        int row = 0;
        try{
            String sql = "Delete FROM receipt WHERE id = ?";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            
            row = stmt.executeUpdate();
            System.out.println("Affect row "+row);
        } catch (SQLException ex) {
            System.out.println("Error: can't delete receipt id : "+id);
        }
        db.close();
        return row;
    }

    @Override
    public int update(Receipt object) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    public static void main(String[] args) {
        Product p1 = new Product(1,"greentea",30);
        Product p2 = new Product(2,"thaitea",30);
        Employee seller = new Employee(1,"boy","0955555555","123");
        Customer customer = new Customer(1,"king","0812345678",0);
        Receipt receipt = new Receipt(seller,customer);
        receipt.addReceiptDetail(p1, 1);
        receipt.addReceiptDetail(p2, 3);
//        System.out.println(receipt);
        ReceiptDao dao = new ReceiptDao();
        
        System.out.println("id = "+ dao.add(receipt));
//        System.out.println(receipt);
//        System.out.println(dao.getAll());
        Receipt newReceipt = dao.get(receipt.getId());
        System.out.println("New Receipt : "+ newReceipt);
                
        
    }
}
