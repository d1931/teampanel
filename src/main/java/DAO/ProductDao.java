/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import Model.Product;
import com.kittikun.Database.Database;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author W.Home
 */
public class ProductDao implements DaoInterface<Product>{
    

    @Override
    public int add(Product object) {
        Connection conn = null;
       Database db = Database.getInstance();
       conn = db.getConnection();
       int id = -1;
        try{
            String sql = "INSERT INTO product (name,price) VALUES(?,?)";
            PreparedStatement stmt = conn.prepareStatement(sql);
            
            stmt.setString(1,object.getName());
            stmt.setDouble(2, object.getPrice());
            int row = stmt.executeUpdate();
            
            ResultSet result = stmt.getGeneratedKeys();   
            
            if(result.next()){
                id = result.getInt(1);
            }
            
            
        } catch (SQLException ex) {
            Logger.getLogger(ProductDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        
        return id;
    }

    @Override
    public ArrayList<Product> getAll() {
        ArrayList<Product> list = new ArrayList();
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
       try{
           String sql = "SELECT * FROM product ";
           Statement stmt = conn.createStatement();
           ResultSet result = stmt.executeQuery(sql);
           while(result.next()){
               int pid = result.getInt("id");
               String name = result.getString("name");
               double price = result.getDouble("price");
               Product product = new Product(pid,name,price);
               list.add(product);
               
           }
       } catch (SQLException ex) {
            Logger.getLogger(ProductDao.class.getName())
                    .log(Level.SEVERE, null, ex);
        }
       
       db.close();
      
       return list;
    }

    @Override
    public Product get(int id) {
       Connection conn = null;
       Database db = Database.getInstance();
       conn = db.getConnection();
       try{
           String sql = "SELECT * FROM product WHERE id ="+id;
           Statement stmt = conn.createStatement();
           ResultSet result = stmt.executeQuery(sql);
           if(result.next()){
               int pid = result.getInt("id");
               String name = result.getString("name");
               double price = result.getDouble("price");
               Product product = new Product(pid,name,price);
               return product;
               
           }
       } catch (SQLException ex) {
            Logger.getLogger(ProductDao.class.getName())
                    .log(Level.SEVERE, null, ex);
        }
       
       db.close();
       return null;
    }
    public Product getByName(String nameIn ){
       Connection conn = null;
       Database db = Database.getInstance();
       conn = db.getConnection();
        try{
           String sql = "SELECT * FROM product WHERE name ="+"\""+nameIn+"\"";
           Statement stmt = conn.createStatement();
           ResultSet result = stmt.executeQuery(sql);
           if(result.next()){
               int pid = result.getInt("id");
               String name = result.getString("name");
               double price = result.getDouble("price");
               Product product = new Product(pid,name,price);
               return product;
               
           }
       } catch (SQLException ex) {
            Logger.getLogger(ProductDao.class.getName())
                    .log(Level.SEVERE, null, ex);
        }
       
       db.close();
       return null;
        
    }

    @Override
    public int delete(int id) {
       Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        int row = 0;
        try{
            String sql = "Delete FROM product WHERE id = ?";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            
            row = stmt.executeUpdate();
            System.out.println("Affect row "+row);
        } catch (SQLException ex) {
            Logger.getLogger(ProductDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        db.close();
        return row;
    }

    @Override
    public int update(Product object) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        int row = 0;
        try{
            String sql = "UPDATE product SET name = ?,price = ? WHERE id = ?";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, object.getName());
            stmt.setDouble(2, object.getPrice());
            stmt.setInt(3, object.getId());
            row = stmt.executeUpdate();
            System.out.println("Affect row "+row);
        } catch (SQLException ex) {
            Logger.getLogger(ProductDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        db.close();
        return row;
    }
       
    public static void main(String[] args) {
        ProductDao dao = new ProductDao();
//        System.out.println(dao.getAll());
//        System.out.println(dao.get(1));
//        int id = dao.add(new Product(-1,"Espresso",20.0));
//        System.out.println("id = "+id);
//        Product lastproProduct = dao.get(id);
//        System.out.println(lastproProduct);
//        lastproProduct.setPrice(50);
//        int row = dao.update(lastproProduct);
//        Product UpdateProduct = dao.get(id);
//        System.out.println("UpdateProduct = "+lastproProduct);
//        dao.delete(id);
//        Product delP = dao.get(id);
//        System.out.println("Delete product "+ delP);
          System.out.println(dao.getByName("Green Tea"));

    }
}
