/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import Model.Customer;
import Model.Employee;
import Model.Product;
import com.kittikun.Database.Database;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Mynx
 */
public class EmployeeDAO implements DaoInterface<Employee> {

    @Override
    public int add(Employee object) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        int id = -1;
        try {
            String sql = "INSERT INTO employee (name,username,password) VALUES(?,?,?)";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, object.getName());
            stmt.setString(2, object.getUsername());
            stmt.setString(3, object.getPassword());
            int row = stmt.executeUpdate();

            ResultSet result = stmt.getGeneratedKeys();

            if (result.next()) {
                id = result.getInt(1);
            }

        } catch (SQLException ex) {
            Logger.getLogger(StockDao.class.getName()).log(Level.SEVERE, null, ex);
        }

        return id;
    }

    @Override
    public ArrayList<Employee> getAll() {
        ArrayList<Employee> list = new ArrayList();
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        try {
            String sql = "SELECT * FROM employee ";
            Statement stmt = conn.createStatement();
            ResultSet result = stmt.executeQuery(sql);
            while (result.next()) {
                int eid = result.getInt("id");
                String name = result.getString("name");
                String username = result.getString("username");
                String password = result.getString("password");
                Employee employee = new Employee(eid, name, username, password);
                list.add(employee);

            }
        } catch (SQLException ex) {
            Logger.getLogger(ProductDao.class.getName())
                    .log(Level.SEVERE, null, ex);
        }

        db.close();

        return list;
    }
    public Employee getByIdAndPasswoed(String nameIn , String passwordIn){
       Connection conn = null;
       Database db = Database.getInstance();
       conn = db.getConnection();
       try{
           String sql = "SELECT *  FROM employee   WHERE username = \""+nameIn+"\" AND password = \""+passwordIn+"\";";
//           String sql = "SELECT *  FROM employee   WHERE username = \"?\" AND password = \"?\"";
           Statement stmt = conn.createStatement();
//           PreparedStatement stmt2 = conn.prepareStatement(sql);
//           stmt2.setString(0, nameIn);
//           stmt2.setString(1, passwordIn);
//           stmt2.executeUpdate();
           ResultSet result = stmt.executeQuery(sql);
           
           
           if(result.next()){
                int eid = result.getInt("id");
                String name = result.getString("name");
                String username = result.getString("username");
                String password = result.getString("password");
              
               Employee employee = new Employee(eid, name, username, password);
               return employee;
               
           }
       } catch (SQLException ex) {
            Logger.getLogger(EmployeeDAO.class.getName())
                    .log(Level.SEVERE, null, ex);
        }
       
       db.close();
       return null;
    }

    @Override
    public Employee get(int id) {
         Connection conn = null;
       Database db = Database.getInstance();
       conn = db.getConnection();
       try{
           String sql = "SELECT * FROM employee WHERE id ="+id;
           Statement stmt = conn.createStatement();
           ResultSet result = stmt.executeQuery(sql);
           if(result.next()){
                int eid = result.getInt("id");
                String name = result.getString("name");
                String username = result.getString("username");
                String password = result.getString("password");
              
               Employee employee = new Employee(eid, name, username, password);
               return employee;
               
           }
       } catch (SQLException ex) {
            Logger.getLogger(EmployeeDAO.class.getName())
                    .log(Level.SEVERE, null, ex);
        }
       
       db.close();
       return null;
    }

      @Override
    public int delete(int id) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        int row = 0;
        try{
            String sql = "Delete FROM employee WHERE id = ?";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            
            row = stmt.executeUpdate();
            System.out.println("Affect row "+row);
        } catch (SQLException ex) {
            Logger.getLogger(EmployeeDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        db.close();
        return row;
    }
    public Employee getByName(String nameIn){
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        try{
           String sql = "SELECT * FROM employee WHERE name ="+"\""+nameIn+"\"";
           Statement stmt = conn.createStatement();
           ResultSet result = stmt.executeQuery(sql);
           if(result.next()){
                int eid = result.getInt("id");
                String name = result.getString("name");
                String username = result.getString("username");
                String password = result.getString("password");
              
               Employee employee = new Employee(eid, name, username, password);
               return employee;
               
           }
       } catch (SQLException ex) {
            Logger.getLogger(EmployeeDAO.class.getName())
                    .log(Level.SEVERE, null, ex);
        }
       
       db.close();
       return null;
    }

    @Override
    public int update(Employee object) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        int row = 0;
        try{
            String sql = "UPDATE employee SET name = ?,username = ?,password = ? WHERE id = ?";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, object.getName());
             stmt.setString(2, object.getUsername());
             stmt.setString(3, object.getPassword());
            stmt.setInt(4, object.getId());
            row = stmt.executeUpdate();
            System.out.println("Affect row "+row);
        } catch (SQLException ex) {
            Logger.getLogger(EmployeeDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        db.close();
        return row;
    }
       
    public static void main(String[] args) {
        EmployeeDAO dao = new EmployeeDAO();
//      System.out.println(dao.getByIdAndPasswoed("zone11", "1212"));
//        System.out.println(dao.getByName("Zone"));        
//        System.out.println(dao.getAll());
//        System.out.println(dao.get(1));
////        int id = dao.add(new Employee(-1,"Zone","oza007","101010"));
////        System.out.println("id ="+id);
////        Employee updateEmployee = dao.get(id);
////        System.out.println(updateEmployee);
//////        updateEmployee.setUsername("Zone");
////        int row = dao.update(updateEmployee);
//        Employee currEmployee = dao.get(id);
//        System.out.println("Update   Employee = "+updateEmployee);
//        Employee delC = dao.get(id);
        
        
        
    }
}
