/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

/**
 *
 * @author Mynx
 */
public class Employee {
   private int id;
    private String name;
    private String username;
       private String password;

    public Employee(int id, String name, String username,String password) {
        this.id = id;
        this.name = name;
        this.username = username;
        this.password = password;
    }
    public Employee(String name, String username,String password) {
        this(-1,name,username,password);
    }
     public Employee(int id,String name) {
       this(id,name,"","");
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    
        public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }


    @Override
    public String toString() {
        return " ID : " + id  +" Name : " + name + " Username : "+ username + " Password : "+ password;
    }
}
