/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

/**
 *
 * @author ModG
 */
public class Customer {
    private int id;
    private String name;
    private String tel;
    private int discount;

    public Customer(int id, String name, String tel,int discount) {
        this.id = id;
        this.name = name;
        this.tel = tel;
        this.discount = discount;
    }
    public Customer(String name, String tel) {
        this(-1,name,tel,7);
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTel() {
        return tel;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }

    public int getDiscount() {
        return discount;
    }

    public void setDiscount(int discount) {
        this.discount = discount;
    }
    
    @Override
    public String toString() {
        return " ID : " + id  + " Name : " + name +" Tel : " + tel  + " Discount : "+ discount;
    }
}
