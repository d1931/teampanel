/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

/**
 *
 * @author W.Home
 */
public class Stock {
    private int id;
    private String name ;
    private double price ;
    private int amount;

    public  Stock(int id, String name, double price, int amount) {
        this.id = id;
        this.name = name;
        this.price = price;
        this.amount = amount;
    }
    public Stock(String name, double price, int amount) {
        this(-1,name,price,amount);
    }
    
    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public double getPrice() {
        return price;
    }
    public int getAmount(){
        return amount;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setPrice(double price) {
        this.price = price;
    }
    public void setAmount(int amount){
        this.amount = amount;
    }

    @Override
    public String toString() {
        return "Id : " + id + " Name : " + name + " Price : " + price + " Amount : "+amount ;
    }
    
    
}
