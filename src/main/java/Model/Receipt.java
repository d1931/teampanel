/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.ArrayList;
import javax.swing.JOptionPane;

/**
 *
 * @author TUFGaming
 */
public class Receipt {

    private int id;
    private Date created;
    private Employee seller;
    private Customer customer;
    private ArrayList<ReceiptDetail> receiptDetail;
    private int discount;

    public Receipt(int id, Date created, Employee seller, Customer customer,int discount) {
        this.id = id;
        this.created = created;
        this.seller = seller;
        this.customer = customer;
        this.discount = discount;
        receiptDetail = new ArrayList<>();

    }

    public Receipt(Employee seller, Customer customer) {
        this(-1, null, seller, customer,customer.getDiscount());
    }

    public void addReceiptDetail(int id, Product product, int amount, double price) {
        for (int row = 0; row < receiptDetail.size(); row++) {
            ReceiptDetail r = receiptDetail.get(row);
            if (r.getProduct().getId() == product.getId()) {
                r.addAmount(amount);
                return;
            }
        }
        receiptDetail.add(new ReceiptDetail(id, product, amount, price, this));
    }

    public void addReceiptDetail(Product product, int amount) {
        addReceiptDetail(-1, product, amount,product.getPrice());
    }

    public void deleteReceiptDetail(int row) {
        receiptDetail.remove(row);
    }

    public double getTotal() {
        double total = 0;
        for (ReceiptDetail r : receiptDetail) {
            total = total + r.getTotal();
        }
        return total;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date time) {
        this.created = time;
    }

    public Employee getSeller() {
        return seller;
    }

    public void setSeller(Employee seller) {
        this.seller = seller;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public ArrayList<ReceiptDetail> getReceiptDetail() {
        return receiptDetail;
    }

    public void setReceiptDetail(ArrayList<ReceiptDetail> receiptDetail) {
        this.receiptDetail = receiptDetail;
    }

    public int getDiscount() {
        return discount;
    }

    public void setDiscount(int discount) {
        this.discount = discount;
    }
    
    @Override
    public String toString() {
        int i = 1;
        String str = 
        "Receipt" + "\n"
        + "Seller : "+ seller.getName() + "\n"
        + "Customer : "+ customer.getName() + "\n"
        + "Order No. : " + id+ "\n"
        + "Time : " + created + "\n"
        +"******************************" + "\n"
        +"ReceiptDetail" + "\n"
        +"No."+"\t"+ "Name" + "\t" + "Amount" + "\t" + "Price" + "\t" + "Total" + "\n";
        for (ReceiptDetail r : receiptDetail) {
            
            
            str = str + i +"\t"+r.toString() + "\n";
            i++;
        }
        str = str + "******************************" + "\n";
        str = str + "SubTotal : "+this.getTotal() + "\n";
        str = str + "Discount : "+this.getDiscount()+ "\n"; 
        double rebate = (this.getTotal() * this.getDiscount()) /100;
        str = str + "Total : "+ (this.getTotal()- rebate) + "\n";
        double pay = Double.parseDouble(JOptionPane.showInputDialog(null, "Pay : "));
        str = str + "Change : "+ (pay - this.getTotal());
        return str;
    }
}
